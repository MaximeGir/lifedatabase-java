
package models;

import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import javax.swing.table.DefaultTableModel;

public class DatalifeTableModel {
    DefaultTableModel model;
    public DatalifeTableModel(DBCollection collection){
         String[] columns = {"Quand", "Qui", "Ou", "Quoi", "Feeling"};
         this.model = new DefaultTableModel(columns, 0);
         DBCursor cursor = collection.find();
         while(cursor.hasNext()){
           DBObject obj = cursor.next();
           String quand = (String) obj.get("Quand");
           String qui = (String) obj.get("Qui");
           String ou = (String) obj.get("Ou");
           String quoi = (String) obj.get("Quoi");
           String feeling = (String) obj.get("Feeling");
           this.model.addRow(new Object[] {quand, qui, ou, quoi, feeling});
         }
    }
    public DefaultTableModel getModel(){
      return this.model; 
    }
    
    
}
