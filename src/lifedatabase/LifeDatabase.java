package lifedatabase;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.mongodb.MongoURI;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import lifedatabase.gui.MainWindow;
import models.DatalifeTableModel;

public class LifeDatabase {

    public static void main(String args[]) throws UnknownHostException {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    MongoURI uri = new MongoURI("//connection uri");
                    Mongo client = new Mongo(uri);
                    DB db = client.getDB("app20904510");
                    db.authenticate(uri.getUsername(), uri.getPassword());
                    DBCollection liste = db.getCollection("datalife");
                    DatalifeTableModel model = new DatalifeTableModel(liste);
                    new MainWindow(model.getModel(), db).setVisible(true);
                } catch (UnknownHostException ex) {
                    Logger.getLogger(LifeDatabase.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
